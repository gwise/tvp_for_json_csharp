﻿CREATE OR REPLACE FUNCTION public.usp_set_emp_json (
  p_employee_json json
)
RETURNS void AS
$body$
DECLARE
		_sqlstate    TEXT;
		_message     TEXT;
		_context     TEXT;
BEGIN
	WITH cte AS (SELECT A.emp_id, A.emp_nm FROM json_to_recordset(p_employee_json) AS A(emp_id INTEGER, emp_nm VARCHAR)),
/*
	del as
				(
					 DELETE FROM employee
					 WHERE NOT EXISTS(SELECT 1 FROM cte a WHERE a.emp_id = employee.emp_id)
				),
*/
	upd as
				(
						UPDATE employee
						SET emp_nm = cte.emp_nm
						,	last_chg_time = now()
						FROM cte
						WHERE cte.emp_id = employee.emp_id
						AND cte.emp_nm <> employee.emp_nm
				)
	INSERT INTO employee
	SELECT cte.emp_id, cte.emp_nm, now(), now()
	FROM cte
	WHERE NOT EXISTS(SELECT 1 FROM employee  WHERE employee.emp_id = cte.emp_id);

 EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
    	_sqlstate = RETURNED_SQLSTATE, _message =  MESSAGE_TEXT, _context = PG_EXCEPTION_CONTEXT;
      RAISE EXCEPTION 'sqlstate: %, message: %, context: [%]',_sqlstate, _message, replace(_context, E'n', ' <- ');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;