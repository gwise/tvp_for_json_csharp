TVP For JSON 일경우 Class선언할때 Table의 이름과 동일하게 해야 한다.


namespace TVP
{
    /// <summary>
    /// Postgresql의 Composite Type과 이름까지 동일하게
    /// </summary>
    /// 
    public class EmployeeUdt
    {
        public int emp_id{ get; set; } //pg와 달라도 상관없음.

        public string emp_nm{ get; set; }
    }
}