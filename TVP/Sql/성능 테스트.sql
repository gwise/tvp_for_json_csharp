﻿truncate table employee;

select emp_id, date_part('day',first_in_time) from employee 
order by 1

select 
    date_part('month',first_in_time),
	date_part('day',first_in_time), 
	min(first_in_time) + ((max(first_in_time) - min(first_in_time))/ count(*)) from employee
group by date_part('month',first_in_time),
		date_part('day',first_in_time)
order by 1

SELECT      avg(date_part('HOUR', first_in_time )) as AVERAGE_HOUR,
            stddev_pop(date_part('HOUR', first_in_time )) as STDEV_HOUR

FROM        employee


update employee
set
 first_in_time = '2018-11-07 10:19:36'
where EMP_ID = 5

insert into employee
 SELECT
    gs as idx,
    --'테스트 문자열' || gs AS test_string,
    left(md5(random()::text), 10) AS random_string
FROM
    generate_series(1, 5) AS gs;


select * from employee;

vacuum full freeze analyze employee;


WITH stuff AS
(
SELECT
    array['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'] as names,
    array['USA', 'UK', 'UAE', 'IND', 'GER', 'RUS'] AS cnames
)
SELECT
    generate_series(1, 1000) id,
    names[(random()*100)::int%8+1] cname,
    cnames[(random()*100)::int%6+1] countrycode,
    ltrim(round(random()::numeric, 10)::text, '0.') contactnumber
FROM stuff;


select * from employee;