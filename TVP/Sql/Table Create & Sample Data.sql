
CREATE TABLE public.employee (
  emp_id INTEGER NOT NULL,
  emp_nm VARCHAR(40),
  first_in_time TIMESTAMP WITH TIME ZONE DEFAULT clock_timestamp(),
  last_chg_time TIMESTAMP WITH TIME ZONE DEFAULT clock_timestamp(),
  CONSTRAINT employee_pkey PRIMARY KEY(emp_id)
)
WITH (oids = false);

CREATE TYPE public.employee_udt AS (
  emp_id INTEGER,
  emp_nm VARCHAR(40)
);

truncate table employee;

insert INTO employee(emp_id, emp_nm)
VALUES (1, 'gwise'), (2, 'jinsun'), (3, 'sunju');

select * from employee;