﻿using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace TVP
{
    public partial class Form1 : Form
    {
        private string connstring = "Server=127.0.0.1;User Id=postgres;Password=0152;Database=postgres;Minimum Pool Size=10;Maximum Pool Size=100;Application Name=WMS_APP";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var _param = new[] 
            {
                new NpgsqlParameter
                {
                    ParameterName = "p_emp_nm",
                    NpgsqlDbType = NpgsqlDbType.Varchar,
                    NpgsqlValue = this.txtEmpNm.Text.Trim()
                }
            };

            string jsonString = SqlHelper.ExecuteReader(
                this.connstring, 
                CommandType.StoredProcedure, 
                "usp_get_emp_json", _param);

            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonString);
            DataView dv = new DataView(dt)
            {
                Sort = "emp_id"
            };

            this.dataGridView1.DataSource = dv.ToTable();

            MessageBox.Show("Search");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<EmployeeUdt> lst_param = new List<EmployeeUdt>();

            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if (this.dataGridView1[0, i].Value != null && 
                    !string.IsNullOrEmpty(this.dataGridView1[0, i].Value.ToString()))
                {
                    lst_param.Add
                    (
                        new EmployeeUdt
                        {
                            emp_id = Convert.ToInt32(this.dataGridView1[0, i].Value),
                            emp_nm = this.dataGridView1[1, i].Value.ToString()
                        }
                    );
                }
            }

            string jsonString = JsonConvert.SerializeObject(lst_param);

            var _param = new[] 
            {
                new NpgsqlParameter
                {
                    ParameterName="p_employee_json",
                    NpgsqlDbType = NpgsqlDbType.Json,
                    NpgsqlValue = jsonString
                }
            };

            SqlHelper.ExecuteNonQuery(this.connstring, "usp_set_emp_json", _param);

            MessageBox.Show("Save");
        }
    }
}
