﻿namespace TVP
{
    /// <summary>
    /// JSON 사용할 경우 Postgresql 함수에서 
    /// 사용하는 이름(대소문자)과 동일하게 설정한다.
    /// </summary>
    public class EmployeeUdt
    {
        public int emp_id{ get; set; }

        public string emp_nm{ get; set; }
    }
}


