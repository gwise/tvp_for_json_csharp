﻿using Npgsql;
using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace TVP
{
    public class SqlHelper
    {
        public SqlHelper()
        {

        }

        public static bool ExecuteNonQuery(string connectionString, string query, 
            params NpgsqlParameter[] sqlParam)
        {
            bool result = false;

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand cmdSql = new NpgsqlCommand(query, connection))
                {
                    cmdSql.CommandTimeout = 180; 
                    cmdSql.CommandType = CommandType.StoredProcedure;
                    cmdSql.Parameters.AddRange(sqlParam);
                    try
                    {
                        cmdSql.ExecuteNonQuery();
                        result = true;
                    }
                    catch (NpgsqlException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            return result;
        }

        public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText, params NpgsqlParameter[] sqlParam)
        {
            DataSet ds = new DataSet();

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                using (NpgsqlCommand cmdSql = new NpgsqlCommand(commandText, connection))
                {
                    cmdSql.CommandTimeout = 180; 
                    cmdSql.CommandType = commandType;
                    cmdSql.Parameters.AddRange(sqlParam);
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmdSql))
                    {
                        da.Fill(ds);
                    }
                }
            }
            return ds;
        }
        
        public static string ExecuteReader(string connectionString, CommandType commandType, 
            string commandText, params NpgsqlParameter[] sqlParam)
        {
            string result = string.Empty;
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand cmdSql = new NpgsqlCommand(commandText, connection))
                {
                    cmdSql.CommandTimeout = 180;
                    cmdSql.CommandType = commandType;
                    cmdSql.Parameters.AddRange(sqlParam);
                    using (var reader = cmdSql.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = reader.GetString(0);
                        }
                    }
                }
            }
            return result;
        }
    }
}
